# Introduction to CNN using Keras

## Description :

Ce projet est une introduction aux réseaux de neurones convolutifs. Le but est de prédire la classe d'appartenance d'une image donnée.
Dans notre cas, chaque classe représente un vêtement particulier ( chassures, t-shirt, pantalon...).

Pour se faire, nous étudierons d'abord notre jeu de données, avec un peu d'exploration statistiques puis nous viendrons traiter nos données pour pouvoir les entrer dans le réseau de neurones convolutif.

## Installation :

De votre coté, sur n'importe quelle plateforme utilisant des notebooks, vous pourrez utiliser le notebook.
